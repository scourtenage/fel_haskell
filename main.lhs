>module Fel where

>import Data.Char
>import Data.List

>import Utils

>main = do
>	expr <- getLine
>	putStrLn (runProg expr)

>runProg :: String -> String
>runProg = showResults . eval . compile . parse . fellex

Lexical analysis function fellex (so named to distinguish it from the built-in lex function).

>type Token = String

>fellex::String -> [Token]
>fellex (c:cs) | c == '\\'  = "\\" : fellex cs
>	 | elem c ['(',')','.',':','=',';'] = [c] : fellex cs -- [c] is necessary to convert char to string
>	 | c == ' '  = fellex cs
>	 | isAlpha(c) = var : fellex restcs
>	 | isDigit(c) = num : fellex remcs
>	 | elem c ['&','|','>'] = op : fellex resttoks 
>	 | otherwise = fellex cs
>			where
>			(var,restcs) = span isAlpha (c:cs)
>			(num,remcs)  = span isDigit (c:cs)
>			(op,resttoks) = span (c==) (c:cs)
>fellex [] = []

The syntax of FEL expressions.  An FEL expression can be a variable, a number
(integer for now), an application of two FEL expressions, and a lambda
expression, made up of a pattern (which holds typed parameters) and an
expression body.

>data Expr a
>    = Var a
>      | Literal a
>      | Type a
>      | Num Int
>      | App (Expr a) (Expr a)
>      | Lambda (Expr a) (Expr a)
>      | PAtom (Expr a) (Expr a)
>      | Patt PatternOp (Expr a) (Expr a)
>    deriving (Show)

>data PatternOp = Disj | Seq | Conj deriving (Show)

>type Name = String

>type FELExpr = Expr Name

What is an FEL program?  It is a list of supercombinator expressions. A supercombinator is a kind
of named lambda abstraction with no free variable, which can be invoked or called using that name.

>type Program a = [SuperComb a]
>type FELProgram = Program Name

>type SuperComb a = (Name,[a],Expr a)
>type FELSuperComb = SuperComb Name

--- Parsing ----

A parser is something that takes a list of tokens and transforms it into a list of possible somethings
together with any remaining tokens

>type Parser a = [Token] -> [(a,[Token])]

Top-level parsing function.  The program is actually parsed by parseExpr, but that function returns
all possible parses as a list of parsed programs and left-over tokens.  We want the first complete
parse, which means the first one that uses up all the tokens. This is found using 'take_first_parse'.
If there is no parse that uses all the tokens, then we report a syntax error.

-- TO DO
OK this needs to be changed so that we produce a FEL program.  A FEL program will be (at least) a
supercombinator (main?).  We also need to introduce a lambda abstraction rule in the compilation section,
and type-checking!
--


>parse :: [Token] -> FELProgram
>parse ts = take_first_parse (parseExpr ts) . parseProgram
>	    where
>	    take_first_parse ((prog,[]): _)   = prog
>	    take_first_parse (parse : others) = take_first_parse others
>	    take_first_parse others	      = error "Syntax error"

parseProgram parses the supercombinators that make up an FEL program

>parseProgram :: Parser FELProgram
>parseProgram = pOneOrMoreWithSep parseSuperComb (pLit ";")

This definition of parseSuperComb is based on the one from Peyton Jones etc.  The arguments
to the supercombinator are plain variables in Peyton Jones etc's Core language, but in FEL,
a supercombinator has a pattern as argument.

>parseSuperComb :: Parser FELSuperComb
>parseSuperComb = pThen4 mk_sc parseVar parsePattern (pLit "=") parseExpr
>		  where
>		  mk_sc sc_name sc_patt eqs sc_body = (sc_name,sc_patt,sc_body)

parseExpr implements the FEL grammar using parse functions

>parseExpr :: Parser FELExpr
>parseExpr = parseVar `pAlt` parseNum `pAlt` parseLambda `pAlt` parseApp

We expect variable names to be alphabetic only - for now

>parseVar :: Parser FELExpr
>parseVar (t:ts) | foldl (&&) True (map isAlpha t) = [((Var t),ts)]
>		 | otherwise			    = []
>parseVar ts            = []

parseType is the same as parseVar with one difference - the first character must be an upper-case letter

>parseType :: Parser FELExpr
>parseType (t:ts) | isUpper(head t) && (foldl (&&) True (map isAlpha t)) = [((Type t),ts)]
>parseType ts            = []


>parseNum :: Parser FELExpr
>parseNum (d:ts) = getresult (stringToInt d)
>		   where
>		   getresult (Just i)	= [(Num i,ts)]
>		   getresult Nothing	= []
>parseNum []     = []

parseLambda 

>parseLambda:: Parser FELExpr
>parseLambda = pThen4 mk_lambda (pLit "\\") parsePattern (pLit ".") parseExpr
>	       where
>	       mk_lambda a b c d = (Lambda b d)

A pattern is either a typed variable, such as x:T, or a compound pattern, such as
(x:T & y:U) or (x:T > y:U)

>parsePattern :: Parser FELExpr
>parsePattern = parsePatternVar `pAlt` parseCompoundPattern

>parsePatternVar :: Parser FELExpr
>parsePatternVar = pThen3 mk_patternvar parseVar (pLit ":") parseType
>		   where mk_patternvar a b c = PAtom a c

>parseCompoundPattern :: Parser FELExpr
>parseCompoundPattern
>   = pThen5 mk_compound (pLit "(") parsePattern parsePatternOp parsePattern (pLit ")")
>     where mk_compound a b c d e = Patt c b d

>parsePatternOp :: Parser PatternOp
>parsePatternOp (t:ts) | t == "||" = [(Disj,ts)]
>		       | t == "&&" = [(Conj,ts)]
>	 	       | t == ">>"  = [(Seq,ts)]
>parsePatternOp ts                   = []

Parse applications in form (E1)E2

>parseApp :: Parser FELExpr
>parseApp = pThen4 mk_app (pLit "(") parseExpr (pLit ")") parseExpr
>	    where
>	    mk_app a b c d = App b d

Parser utility functions
------------------------

pLit parses literals

>pLit :: String -> Parser String
>pLit s (t:ts) | s == t    = [(s,ts)]
>	       | otherwise = []
>pLit s [] = []

pAlt allows different alternative parses to be followed

>pAlt :: Parser a -> Parser a -> Parser a
>pAlt p1 p2 toks = (p1 toks) ++ (p2 toks)


pThen combines two parsers, which operate in sequence, into a larger parser

>pThen:: (a -> b -> c) -> Parser a -> Parser b -> Parser c
>pThen combine p1 p2 toks
>	= [(combine v1 v2,toks2) | (v1,toks1) <- p1 toks, (v2,toks2) <- p2 toks1]

pThen3 combines three parsers, which operate in sequence, into a larger parser

>pThen3:: (a -> b -> c -> d) -> Parser a -> Parser b -> Parser c -> Parser d
>pThen3 combine p1 p2 p3 toks
>	= [(combine v1 v2 v3,toks3)
>		| (v1,toks1) <- p1 toks, (v2,toks2) <- p2 toks1, (v3,toks3) <- p3 toks2]

>pThen4 :: (a -> b -> c -> d -> e) -> Parser a -> Parser b -> Parser c
>          -> Parser d -> Parser e
>pThen4 combine p1 p2 p3 p4 toks
>	= [(combine v1 v2 v3 v4,toks4)
>		| (v1,toks1) <- p1 toks,
>		  (v2,toks2) <- p2 toks1,
>		  (v3,toks3) <- p3 toks2,
>		  (v4,toks4) <- p4 toks3
>	  ]

>pThen5 :: (a -> b -> c -> d -> e -> f) -> Parser a -> Parser b -> Parser c
>          -> Parser d -> Parser e -> Parser f
>pThen5 combine p1 p2 p3 p4 p5 toks
>	= [(combine v1 v2 v3 v4 v5,toks5)
>		| (v1,toks1) <- p1 toks,
>		  (v2,toks2) <- p2 toks1,
>		  (v3,toks3) <- p3 toks2,
>		  (v4,toks4) <- p4 toks3,
>		  (v5,toks5) <- p5 toks4
>	  ]
		
stringToInt is a general utility function for converting tokens to integers.  Returns
an Int (wrapped in Just) if converted or Nothing if not converted

>stringToInt:: Token -> Maybe Int
>stringToInt d = checkresult (reads d :: [(Int,String)])
>		 where
>		 checkresult []       = Nothing
>		 checkresult [(i,"")] = Just i
>		 checkresult [(i,s)]  = Nothing

>pEmpty :: a -> Parser a
>pEmpty t toks = [(t,toks)]

>pZeroOrMore :: a -> Parser [a]
>pZeroOrMore p = (pOneOrMore p) `pAlt` pEmpty

>pOneOrMore :: a -> Parser [a]
>pOneOrMore p toks = pThen combine p (pZeroOrMore p) toks
>		     where
>		     combine a b = (a : b) 

>pOneOrMoreWithSep :: Parser a -> Parser b -> Parser [a]
>pOneOrMoreWithSep p1 p2 []
>	= []
>pOneOrMoreWithSep p1 p2 toks
>	= pOneOrMore (pThen (\x y -> x) p1 p2) toks

Compilation
-----------

Compile an FEL expression into a TI state.  This section (and the evaluation section)
uses the Template Instantiation method of compiling and evaluating functional languages
from SPJ's and ML's book.

>type TiState = (TiStack, TiDump, TiHeap, TiGlobals, TiStats)

Stack (or Spine Stack) is a list of heap addresses

>type TiStack = [Addr]

The Template Instantiation method from the book doesn't use the Dmp
but it is included for later chapters.  Need to decide if we are
going to update this code in line with later chapters of the book -
otherwise we could remove it.

>data TiDump = DummyTiDump
>initialTiDump = DummyTiDump

>type TiHeap = Heap Node

-- TO DO
This needs some work, because supercombinators don't have patterns in this structure - but they
ought to.  Should patterns be a different type?

>data Node = NAp Addr Addr			    -- Application
>	      | NSupercomb Name [Name] FELExpr -- Supercombinator (BUT NO PATTERN!)
>	      | NNum Int			    -- A number
>	      | NLam FELExpr FELExpr	    -- Lambda abstraction (not sure about this)

>type TiGlobals = ASSOC Name Addr

Template Instantiation statistics

>type TiStats = Int

>tiStatInitial :: TiStats
>tiStatInitial = 0

>tiStatIncSteps :: TiStats -> TiStats
>tiStatIncSteps s = s+1

>tiGetSteps :: TiStats -> Int
>tiGetSteps s = s

>applyToStats :: (TiStats -> TiStats) -> TiState -> TiState
>applyToStats stats_fun (stack,dump,heap,sc_defs,stats)
>	= (stack,dump,heap,sc_defs,stats_fun stats)



>compile :: FELExpr -> TiState
>compile program
>	= (initial_stack,initialTiDump, initial_heap,globals,tiStatInitial)
>	  where
>	  sc_defs = program ++ preludeDefs
>	  (initial_heap,globals) = buildInitialHeap sc_defs
>	  initial_stack = [address_of_main]
>         address_of_main = aLookup globals "main" (error "main is not defined")

>preludeDefs = [] -- no prelude for now

>buildInitialHeap :: [FELExpr] -> (TiHeap, TiGlobals)
>buildInitialHeap sc_defs = mapAccuml allocateSc hInitial sc_defs

*** We need to change this for FEL lambda expressions ***

>allocateSc :: TiHeap -> FELExpr -> (TiHeap,(Name,Addr))
>allocateSc heap (name,args,body)
>   = (heap',(name,addr))
>     where
>     (heap',addr) = hAlloc heap (NSupercomb name args body)

Evaluation functions
--------------------

>eval :: Expr a -> Expr a
>eval e =  e

>showResults :: Expr a -> String
>showResults e = "Expr"
